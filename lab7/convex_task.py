import numpy as np 
from scipy.optimize import linprog


def gradient_in_point(D, c, x):
	return np.sum(D * x, axis=1) + c


def function_in_point(func, x):
	D, c, alpha = func
	return c.T.dot(x) + 0.5 * x.dot(D).dot(x.T) + alpha


def is_suboptimal_plan(grad, c, D, alpha, x_tilde):
	n = len(x_tilde)

	is_active = [function_in_point(i, x_tilde) == 0 for i in zip(D, c, alpha)]

	A = [gradient_in_point(D[i], c[i], x_tilde) for i in range(len(is_active)) if is_active[i]]

	b = [0] * len(A)

	bounds = []
	for i in range(n):
		if x_tilde[i] == 0:
			pair = (0, 1)
		else:
			pair = (-1, 1)
		bounds.append(pair)

	res = linprog(c=grad, A_ub=A, b_ub=b, bounds=bounds)

	if res.fun == 0 and res.success:
		return None
	else:
		return res.x


def is_valid_plan(D, C, alpha, x):
	for (d, c, a) in zip(D, C, alpha):
		f = c.dot(x.T) + 0.5 * x.dot(d).dot(x.T) + a
		if f > 0:
			return False
	return True


def build_new_plan(D_, c_, c, D, alpha, x_tilde, x_line):
	n = len(x_tilde)
	grad = gradient_in_point(D_, c_, x_tilde)
	l_0 = is_suboptimal_plan(grad, c, D, alpha, x_tilde)
	if l_0 is None:
		return {
			"message": "x_tilde is optimal plan",
			"x": x_tilde
		}

	_b = (x_line - x_tilde).dot(grad.T)

	_a = grad.dot(l_0.T)

	if _b > 0:
		a =  -_a / 2 / _b
	else:
		a = 1

	t = 1

	f_x_tilde = c_.dot(x_tilde.T) + 0.5 * x_tilde.dot(D_).dot(x_tilde.T)

	while True:
		x_new = x_tilde + t * l_0 + a * t * (x_line - x_tilde)

		if is_valid_plan(D, c, alpha, x_new):
			f_x_new = c_.dot(x_new.T) + 0.5 * x_new.dot(D_).dot(x_new.T)
			if f_x_new < f_x_tilde:
				return {
					"message": "new plan is found",
					"x": x_new
				}
		t /= 2
