import numpy as np
from convex_task import *

D_ = np.array([
		[2, 1],
		[1, 2]
	])

c_ = np.array([-3, -3])


c1 = [1, -1]
c2 = [-1, 1]


D1 = [
		[1, 0],
		[0, 1]
]

D2 = [
	[1, 0.5],
	[0.5, 1]
]

alpha1 = -0.5
alpha2 = -1.5

c = np.array([c1, c2])
D = np.array([D1, D2])
alpha = np.array([alpha1, alpha2])


x_tilde = np.array([0, 1])
x_line = np.array([0, 0])

result = build_new_plan(
		D_=D_,
		c_=c_,
		c=c,
		D=D,
		alpha=alpha,
		x_tilde=x_tilde,
		x_line=x_line
)
