import numpy as np

def get_invert_matrix(A, A_, x, i):

	l = A_.dot(x)
	if l[i] == 0:
		return -1

	l1 = np.array(l)
	l1[i] = -1

	l2 = (-1. / l[i]) * l1

	Q = np.eye(len(A))
	for j in range(len(A)):
		Q[j][i] = l2[j]

	return Q.dot(A_)


if __name__ == '__main__':
	A = np.array([[1, 0, 5],
				  [2, 1, 6],
				  [3, 4, 0]])
	A_ = np.array([[-24, 20, -5],
				   [18, -15, 4],
				   [5, -4, 1]])
	print(A.dot(A_))
	x = np.array([[2], [2], [2]])
	i = 1
	print(get_invert_matrix(A, A_, x, i))

