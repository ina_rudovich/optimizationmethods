import numpy as np

def dual_simplex_method(A, b, c, J_b):

    m, n = A.shape
    J = range(n)
    A_b = A[:, J_b]
    A_b_invert = np.linalg.inv(A_b)

    y = c[J_b].T.dot(A_b_invert)

    while True:
        A_b = A[:, J_b]
        A_b_invert = np.linalg.inv(A_b)

        kappa = [0]*n
        kappa_b = A_b_invert.dot(b)

        for i in range(len(J_b)):
            kappa[J_b[i]] = kappa_b[i]

        if all(kappa_b > 0):
            return kappa

        k = np.where(kappa_b == min(kappa))[0][0]

        delta_y = A_b_invert[k]

        J_nb = np.array([i for i in range(n) if i not in J_b])


        mu = np.array([delta_y.dot(A[:, j]) for j in J_nb])

        if all(mu >= 0):
            raise Exception("task is't joint")

        J_nb_negative = J_nb[np.where(mu < 0)]

        indexes = np.where(mu < 0)[0]

        sigma = (c[J_nb] - y.dot(A[:,J_nb]))[indexes] / mu[indexes]

        J_b[k] = J_nb_negative[sigma.argmin()]
        sigma_0 = sigma.min()

        y += sigma_0 * delta_y
