import numpy as np
from lab4 import dual_simplex_method

A = np.array([
    [-2,-1,1,-7,0,0,0,2],
    [4,2,1,0,1,5,-1,-5],
    [1,1,0,-1,0,3,-1,1]
    ])

c = np.array([2, 2, 1, -10, 1, 4, -2, -3])
# x = np.array([4,0,0,6,2,0,0,5])

J_b = np.array([1,4,6])

b = np.array([-2,4,3])



print(dual_simplex_method(A, b, c, J_b))