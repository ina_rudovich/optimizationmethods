import numpy as np
from lab4 import dual_simplex_method

A = np.array([
    [-2,-1,-4,1,0],
    [-2,-2,-2,0,1]
    ])

c = np.array([-4,-3,-7,0,0])
# x = np.array([4,0,0,6,2,0,0,5])

J_b = np.array([3,4])

b = np.array([-1,-1.5])



print(dual_simplex_method(A, b, c, J_b))