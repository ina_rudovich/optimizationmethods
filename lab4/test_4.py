import numpy as np
from lab4 import dual_simplex_method

A = np.array([
    [-2,-1,1,-7,1,0,0,2],
    [-4,2,1,0,5,1,-1,5],
    [1,1,0,1,4,3,1,1]
    ])

c = np.array([12, -2,-6, 20, -18, -5, -7, -20])
# x = np.array([4,0,0,6,2,0,0,5])

J_b = np.array([1,3,5])

b = np.array([-2,8,-2])



print(dual_simplex_method(A, b, c, J_b))