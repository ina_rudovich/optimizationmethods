import numpy as np
from lab4 import dual_simplex_method

A = np.array([
    [3,-1,10,-7,1,0,0,2],
    [7,-2,14,8,0,12,-11,0],
    [1,1,0,1,-4,3,-1,1]
    ])

c = np.array([36, -12, 66, 76, -5, 77, -76, -7])
# x = np.array([4,0,0,6,2,0,0,5])

J_b = np.array([6,7,3])

b = np.array([2,5,-2])



print(dual_simplex_method(A, b, c, J_b))