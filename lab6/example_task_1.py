from main import *
import numpy as np

A = np.array([
		[1., 2., 0., 1., 0., 4., -1., -3.],
		[1., 3., 0., 0., 1., -1., -1., 2.],
		[1., 4., 1., 0., 0., 2., -2., 0.]
	])

b = np.array([4., 5., 6.])

B = np.array([
		[1., 1., -1., 0., 3., 4., -2., 1.],
		[2., 6., 0., 0., 1., -5., 0., -1.],
		[-1., 2., 0., 0., -1., 1., 1., 1.]
	])

d_ = np.array([7., 3., 3.])

D = B.T.dot(B)
print(D)

c = -d_.dot(B)
print(c)

x = np.array([0.,0.,6.,4.,5.,0.,0.,0.])
J_r = np.array([2, 3, 4])
J_r_extended = np.array([2, 3, 4])
input()
print("*************************************\n","ANSWER\n", solve_task(
	A=A,
	b=b,
	c=c,
	D=D,
	x=x,
	J_r=J_r,
	J_r_extended=J_r_extended
	))