from main import *
import numpy as np

A = np.array([
		[11, 0, 0, 1, 0, -4, -1, 1],
		[1, 1, 0, 0, 1, -1, -1, 1],
		[1, 1, 1, 0, 1, 2, -2, 1]
	])

b = np.array([8, 2, 5])

B = np.array([
		[1, -1, 0, 3, -1, 5, -2, 1],
		[2, 5, 0, 0, -1, 4, 0, 0],
		[-1, 3, 0, 5, 4, -1, -2, 1]

	])

d_ = np.array([6, 10, 9])

D = B.T.dot(B)
print(D)

c = -d_.dot(B)
print(c)

x = np.array([0.7273, 1.2727, 3.0000, 0, 0, 0, 0, 0])
J_r = np.array([0, 1, 2])
J_r_extended = np.array([0, 1, 2])
input()
print("*************************************\n","ANSWER\n", solve_task(
	A=A,
	b=b,
	c=c,
	D=D,
	x=x,
	J_r=J_r,
	J_r_extended=J_r_extended
	))