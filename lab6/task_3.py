from main import *
import numpy as np

A = np.array([
		[0, 2, 1, 4, 3, 0, -5, -10],
		[-1, 3, 1, 0, 1, 3, -5, -6],
		[1, 1, 1, 0, 1, -2, -5, 8]
	])

b = np.array([6, 4, 14])


D = np.array([
	[1, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 1, 0, 0, 0, 0],
	[0, 0, 0, 0, 1, 0, 0, 0],
	[0, 0, 0, 0, 0, 1, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 1]
	])

c = np.array([1, 3, -1, 3, 5, 2, -2, 0])
print(c)

x = np.array([0, 2, 0, 0, 4, 0, 0, 1])
J_r = np.array([1, 4, 7])
J_r_extended = np.array([1, 4, 7])
input()
print("*************************************\n","ANSWER\n", solve_task(
	A=A,
	b=b,
	c=c,
	D=D,
	x=x,
	J_r=J_r,
	J_r_extended=J_r_extended
	))