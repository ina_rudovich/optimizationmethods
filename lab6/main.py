import numpy as np


def solve_task(A,b,c,D, x, J_r, J_r_extended):
	m, n = A.shape
	to_step_3 = False
	iteration = 0
	while True:
		if not to_step_3:
			to_step_3 = False
			iteration += 1
			# print("======================================================================")
			# print("ITERATION ", iteration)
			# print("======================================================================")
			# step 1
			c_x = c + D.dot(x)
			print("\nc_x\n",c_x)
			A_r = A[:, J_r]
			A_r_inv = np.linalg.inv(A_r)
			u_x = -c_x[J_r].dot(A_r_inv)
			print("\nu_x\n", u_x)

			delta = u_x.dot(A) + c_x
			print("\ndelta\n", delta)
			J_n = [j for j in range(n) if j not in J_r_extended]
			# print("\nJ_n =",J_n)

			# step 2
			if np.all(delta[J_n] >= 0):
				print(c.T.dot(x) + 0.5 * x.T.dot(D).dot(x))
				return x

			j_0 = np.argmin(delta)
			# print("\nj_0 =",j_0)

		# step 3
		l = np.array([0.] * n)
		l[j_0] = 1
		# print("l =", l)

		# print("\nD**=\n",D[J_r_extended][:,J_r_extended])

		H = np.concatenate(
				(
					np.concatenate((D[J_r_extended][:,J_r_extended], A[ : , J_r_extended].T), axis=1),
					np.concatenate((A[ : , J_r_extended], np.zeros((m, m))), axis=1)
				),
				axis=0
			)
		# print("\nH =\n", H)
		H_inv = np.linalg.inv(H)
		# print("\nH_inv\n", H_inv)

		K = D[:, j_0][J_r_extended]
		L = A[:, j_0]
		bb = -np.concatenate((K, L), axis=0)
		# print("\nbb =\n", bb)

		y = H_inv.dot(bb)
		# print("\ny =\n", y)

		for i in range(len(J_r_extended)):
			l[J_r_extended[i]] = y[i]
		# print("\nl =\n", l)
		l = np.round(l, 16)
		# print("\nl =\n", l)

		# step 4
		thetta = [np.Infinity] * n 
		for i in J_r_extended:
			if l[i] < 0:
				thetta[i] = -x[i] / l[i]

		sigma = l.T.dot(D).dot(l)
		# print("\nsigma =", sigma)
		# print("\nround(sigma, 15) =", round(sigma, 10))
		# if round(sigma, 20) == 0:
			# raise Exception("The objective function is not bounded below")

		if round(sigma, 15) > 0:
			thetta[j_0] = np.abs(delta[j_0]) / sigma

		# print("\n thetta =\n", thetta)

		thetta_0 = np.min(thetta)
		if thetta_0 == np.Infinity:
			raise Exception("The objective function is not bounded below")


		j_thetta_0 = np.argmin(thetta)
		# print("\nthetta_0 =", thetta_0)
		# print("\nj_thetta_0 =", j_thetta_0)

		# step 5

		x = x + thetta_0 * l
		# print("\nx_new = ", x)

		# step 6
		# 1
		if j_0 == j_thetta_0:
			# print("\nAAAAAAAAAAAAAAAAAAAAAA\n")
			J_r_extended = np.append(J_r_extended, j_0)
			# print(J_r_extended)
			to_step_3 = False
		# 2
		elif j_thetta_0 in np.setdiff1d(J_r_extended, J_r):
			# print("\nBBBBBBBBBBBBBBBBBBBBBB\n")
			J_r_extended = np.delete(J_r_extended, np.where(J_r_extended == j_thetta_0))
			to_step_3 = True
			delta[j_0] += thetta_0 * sigma
		# 3 - 4
		elif j_thetta_0 in J_r:
			s = list(J_r).index(j_thetta_0)
			# print("\ns =", s)
			# 3
			for j_plus in np.setdiff1d(J_r_extended, J_r):

				if np.all(np.linalg.inv(A[:, J_r]).dot(A[:, j_plus]) != 0):
					# print("\nCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC\n")
					# print("\n j_plus = ",j_plus)
					# print(J_r)
					J_r[s] = j_plus
					J_r_extended = np.delete(J_r_extended, s)
					delta[j_0] += thetta_0 * sigma
					to_step_3 = True
					break
			else:
				# print("\nDDDDDDDDDDDDDDDDDDDDDDDD\n")
				# 4
				J_r[s] = j_0
				J_r_extended[s] = j_0
				to_step_3 = False


		# print("--------------------------")
		# print("\nnew x = ", x)
		# print("\nnew J_r = ", J_r)
		# print("\nnew J_r_extended =", J_r_extended)
		# print("\nto_step_3 = ", to_step_3)
		# input()