from new_main import *
import numpy as np

A = np.array([
		[1, 0, 2, 1],
		[0, 1, -1, 2]
	])

D = np.array([
		[2, 1, 1, 0],
		[1, 1, 0, 0],
		[1, 0, 1, 0],
		[0, 0, 0, 0]
	])

c = np.array([-8, -6, -4, -6])

b = np.array([2, 3])

J_r = np.array([0, 1])
J_r_extended = np.array([0, 1])

x = np.array([2, 3, 0, 0])

print("*************************************\n","ANSWER\n", solve_task(
	A=A,
	b=b,
	c=c,
	D=D,
	x=x,
	J_r=J_r,
	J_r_extended=J_r_extended
	))