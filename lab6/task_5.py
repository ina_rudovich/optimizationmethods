from main import *
import numpy as np

A = np.array([
		[0, 0, 1, 5, 2, 0, -5, -4],
		[1, 1, -1, 0, 1, -1, -1, -1],
		[1, 1, 1, 0, 1, 2, 5, 8]
	])

b = np.array([15, -1, 9])


D = np.array([
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
	])

c = np.array([1, -3, 4, 3, 5, 6, -2, 0])
print(c)

x = np.array([4, 0, 5, 2, 0, 0, 0, 0])
J_r = np.array([0, 2, 3])
J_r_extended = np.array([0, 2, 3])
input()
print("*************************************\n","ANSWER\n", solve_task(
	A=A,
	b=b,
	c=c,
	D=D,
	x=x,
	J_r=J_r,
	J_r_extended=J_r_extended
	))