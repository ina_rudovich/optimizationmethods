from main import *
import numpy as np

A = np.array([
		[2, -3, 1, 1, 3, 0, 1, 2],
		[-1, 3, 1, 0, 1, 4, 5, -6],
		[1, 1, -1, 0, 1, -2, 4, 8]
	])

b = np.array([8, 4, 14])

B = np.array([
		[1, 0, 0, 3, -1, 5, 0, 1],
		[2, 5, 0, 0, 0, 4, 0, 0],
		[-1, 9, 0, 5, 2, -1, -1, 5]
	])

D = B.T.dot(B)
print(D)

c = np.array([-13, -217, 0, -117, -27, -71, 18, -99])
print(c)

x = np.array([0, 2, 0, 0, 4, 0, 0, 1])
J_r = np.array([1, 4, 7])
J_r_extended = np.array([1, 4, 7])
input()
print("*************************************\n","ANSWER\n", solve_task(
	A=A,
	b=b,
	c=c,
	D=D,
	x=x,
	J_r=J_r,
	J_r_extended=J_r_extended
	))