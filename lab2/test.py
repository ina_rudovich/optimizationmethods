import numpy as np
from lab2 import main_stage

A = np.array([
    [1,1,1,1,0],
    [2,2,2,0,1]
    ])

c = np.array(
    [0, 0, 0, -1, -1])

x = np.array([0,0,0,0,0])

J_b = np.array([3,4])

print(main_stage(A, c, x, J_b))