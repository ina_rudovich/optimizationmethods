import numpy as np 
from main import *

a = np.array([20, 30, 25])
b = np.array([10, 10, 10, 10, 10])
c = np.array([
		[2, 8, -5, 7, 10],
		[11, 5, 8, -8, -4],
		[1, 3, 7, 4, 2]
	])

solve = transport_task_solver(a=a,b=b,c=c)

print("RESULT:")
print(solve)
