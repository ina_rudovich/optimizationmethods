import numpy as np 
from main import *

a = np.array([53, 20, 45, 38])
b = np.array([15, 31, 10, 3, 18])
c = np.array([
		[3, 0, 3, 1, 6],
		[2, 4, 10, 5, 7],
		[-2, 5, 3, 2, 9],
		[1, 3, 5, 1, 9]
	])

solve = transport_task_solver(a=a,b=b,c=c)

print("RESULT:")
print(solve)
