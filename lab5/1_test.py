import numpy as np 
from main import *

a = np.array([100, 300, 300])
b = np.array([300, 200, 200])
c = np.array([
		[8, 4, 1],
		[8, 4, 3],
		[9, 7, 5]
	])
# b = np.array([150,   200, 100, 100])
# a = np.array([100,250,200])

solve = transport_task_solver(a=a,b=b,c=c)

print(solve)
