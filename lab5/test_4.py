import numpy as np 
from main import *

a = np.array([13, 5, 7, 9, 10])
b = np.array([20, 5, 6, 11])
c = np.array([
		[2, 6, 8, -3],
		[3, 2, 12, 4],
		[7, 2, 5, 7],
		[9, 2, 14, 9],
		[8, 7, 8, 8]
	])

solve = transport_task_solver(a=a,b=b,c=c)

print("RESULT:")
print(solve)
