import numpy as np 
from main import *

a = np.array([7, 3, 7, 3, 7])
b = np.array([10, 10, 4, 3])
c = np.array([
		[1, 1, -1, -1],
		[0, 0, 2, 6],
		[5, 4, 7, 6],
		[7, 8, 5, 7],
		[2, 5, 10, 2]
	])

solve = transport_task_solver(a=a,b=b,c=c)

print("RESULT:")
print(solve)
print(np.sum(solve[0] * c))
