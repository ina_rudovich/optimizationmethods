import numpy as np 
from main import *

a = np.array([20, 11, 18, 27])
b = np.array([11, 4, 10, 12, 8, 9, 10, 4])
c = np.array([
		[-3, 6, 7, 12, 6, -3, 2, 16],
		[4, 3, 7, 10, 0, 1, -3, 7],
		[19, 3, 2, 7, 3, 7, 8, 15],
		[1, 4, -7, -3, 9, 13, 17, 22]
	])

solve = transport_task_solver(a=a,b=b,c=c)

print("RESULT:")
print(solve)
