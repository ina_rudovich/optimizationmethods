import numpy as np 
from main import *

a = np.array([15, 12, 18, 20])
b = np.array([5, 5, 10, 4, 6, 20, 10, 5])
c = np.array([
		[-3, 10, 70, -3, 7, 4, 2, -20],
		[3, 5, 8, 8, 0, 1, 7, -10],
		[-15, 1, 0, 0, 13, 5, 4, 5],
		[1, -5, 9, -3, -4, 7, 16, 25]
	])

solve = transport_task_solver(a=a,b=b,c=c)

print("RESULT:")
print(solve)
