import numpy as np
from copy import copy


def first_phase(a, b):
    i = 0
    j = 0
    X = np.zeros((a.size, b.size))
    U_b = []
    while i < a.size and j < b.size :
        if len(U_b) == 0 or U_b[-1] != (i, j):
            U_b.append((i, j))

        if b[j] == 0:
            j += 1
        else :
            payment = min(a[i], b[j] - X[i][j])
            X[i,j] += payment
            b[j] -= payment
            a[i] -= payment
            if a[i] == 0:
                i += 1
    return X, U_b


def main_phase(X, U_b, a, b, c):
    m = a.size
    n = b.size

    while True:
        u_v_sparse_matrix = np.zeros((len(U_b) + 1, m + n))
        c_solve = []
        for u_b, i in zip(U_b, range(len(U_b))):
            u_v_sparse_matrix[i][u_b[0]] = 1
            u_v_sparse_matrix[i][m + u_b[1]] = 1
            c_solve.append(c[u_b[0], u_b[1]])

        u_v_sparse_matrix[-1][0] = 1
        c_solve.append(0)
        
        u_v = np.linalg.solve(u_v_sparse_matrix, c_solve)
        u = u_v[: a.size]
        v = u_v[a.size:]

        delta = c - np.array(u).reshape((len(u), 1)) - np.array(v)

        if np.all(delta >= 0):
            return X, U_b

        i_0, j_0 = np.unravel_index(delta.argmin(),delta.shape)

        U_b.append((i_0, j_0))

        graph = create_graph(U_b, m, n)
        cycle = find_cycle(graph)

        compressed_cycle = compress_cycle(U_b, cycle)
        ind = compressed_cycle.index(len(U_b) - 1)
        compressed_cycle = compressed_cycle[ind:]+compressed_cycle[:ind]
        cycle_plus = [compressed_cycle[i] for i in range(len(compressed_cycle)) if i % 2 == 0]
        cycle_minus = [compressed_cycle[i] for i in range(len(compressed_cycle)) if i % 2 == 1]

        X_elements = []

        for i in cycle_minus:
            X_elements.append(X[U_b[i]])
        thetta = np.min(X_elements)
        index_thetta = np.argmin(X_elements)
        for i in cycle_plus:
            X[U_b[i]] += thetta
        for i in cycle_minus:
            X[U_b[i]] -= thetta
        U_b.remove(U_b[cycle_minus[index_thetta]])


def create_graph(U_b, m, n):
    _graph = np.zeros((m, n))
    for ind in U_b:
        _graph[ind[0], ind[1]] = 1

    graph = [[] for i in U_b]
    for u_b in U_b:
        i = u_b[0]
        j = u_b[1]
        k = j + 1
        while k < n:
            if _graph[i, k] != 0:
                graph[U_b.index(u_b)].append(U_b.index((i,k)))
                break 
            k += 1
        k = j - 1   
        while k >= 0:
            if _graph[i, k] != 0:
                graph[U_b.index(u_b)].append(U_b.index((i,k)))
                break
            k -= 1

        k = i + 1
        while k < m:
            if _graph[k, j] != 0:
                graph[U_b.index(u_b)].append(U_b.index((k,j)))
                break 
            k += 1
        k = i - 1   
        while k >= 0:
            if _graph[k, j] != 0:
                graph[U_b.index(u_b)].append(U_b.index((k, j)))
                break
            k -= 1

    return graph


def compress_cycle(U_b, cycle):
    compressed_cycle = []
    i = 0
    j = 0

    while len(compressed_cycle) == 0 or len(compressed_cycle) == 1 or compressed_cycle[0] != compressed_cycle[-1]:
        j %= len(cycle)
        while U_b[cycle[i]][0] == U_b[cycle[j]][0]:
            j += 1
            j %= len(cycle)
        if (j - i) % len(cycle) > 1:
            compressed_cycle.append(cycle[j-1])
            i = (j - 1) % len(cycle)
            j = (j - 1) % len(cycle)
            continue

        while U_b[cycle[i]][1] == U_b[cycle[j]][1]:
            j += 1
            j %= len(cycle)
            
        if (j - i) % len(cycle) > 1:
            compressed_cycle.append(cycle[j-1])
            i = (j - 1) % len(cycle)
            j = (j - 1) % len(cycle)

    return compressed_cycle[:-1]


def find_cycle(graph):
    is_visited = [False] * len(graph)
    parents = []

    def dfs(index):
        is_visited[index] = True
        parents.append(index)
        for i in graph[index]:
            if not is_visited[i]:
                found_cycle = dfs(i)
                if found_cycle:
                    return True

            if i in parents and i != parents[-2]:
                found_cycle = True
                parents.append(i)
                return True

        parents.pop()
        return False

    dfs(0)
    return parents[parents.index(parents[-1])+1:]


def transport_task_solver(a, b, c):
    m = np.shape(a)
    n = np.shape(b)
    a_smaller_b = False
    a_bigger_b = False

    if sum(a) > sum(b):
        a_bigger_b = True
        b = np.append(b,sum(a) - sum(b))
        c = np.concatenate((c, np.zeros(( a.size, 1))),axis=1)
        print(c)
    elif sum(a) < sum(b):
        a_smaller_b = True
        a = np.append(a, sum(b) - sum(a))

    X, U_b = first_phase(copy(a), copy(b))

    return main_phase(X, U_b, a, b, c)