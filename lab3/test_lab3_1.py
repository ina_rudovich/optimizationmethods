import numpy as np
from lab3 import simplex_method

A = np.array([
    [0,1,4,1,0,-3,5,0],
    [1,-1,0,1,0,0,1,0],
    [0,7,-1,0,-1,3,8,0],
    [1,1,1,1,0,3,-3,1]
    ])

c = np.array(
    [-5,-2,3,-4,-6,0,-1,-5])

x = np.array([4,0,0,6,2,0,0,5])

J_b = np.array([0,3,4,7])

b = np.array([6, 10, -2, 15])



print(simplex_method(A, b, c))