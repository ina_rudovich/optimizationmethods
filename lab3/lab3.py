import numpy as np
from lab2 import main_stage


def first_stage(A, b, c):
    m, n = A.shape

    for i in range(m):
        if b[i] < 0:
            b[i] = -b[i]
            A[i] = -A[i]

    A_extended = np.concatenate((A, np.eye(m)), axis=1)

    x_extended = np.concatenate((np.zeros(n), b), axis=0)

    c_extended = np.concatenate((np.zeros(n), -np.ones(m)), axis=0)

    J_b_extended = np.arange(n, n+m)

    x_extended, J_b_extended, A_b_invert = main_stage(
                                    A=A_extended,
                                    c=c_extended,
                                    x=x_extended,
                                    J_b=J_b_extended)

    if np.any(x_extended[n:] != 0):
        raise Exception("Error : Task isn't joint")

    while np.intersect1d(J_b_extended, np.arange(n, n+m)):
        j_k = np.intersect1d(J_b_extended, np.arange(n, n+m))[0]
        k, = np.where(J_b_extended == j_k)[0]
        i = j_k - n + 1
        J_nb_true = [jb for jb in range(n) if jb not in J_b_extended]
        l = A_b_invert.dot(A[:,J_nb_true])
        if np.any(l[k, : ] != 0):
            J_b_extended[k] = J_nb_true(np.where(l[k, : ] != 0)[0])
        else:
            A = np.delete(A, (i - 1), axis=0)
            A_extended = np.delete(A_extended, (i - 1), axis=0)
            J_b_extended = np.delete(J_b_extended, k)
            A_b_invert = np.linalg.inv(A_extended[:,J_b_extended])

    J_b = J_b_extended
    x = x_extended[:n]

    return A, c, x, J_b


def simplex_method(A, b, c):
    A, c, x, J_b = first_stage(A, b, c)
    return main_stage(
                A=A,
                c=c,
                x=x,
                J_b=J_b)[0:2]
