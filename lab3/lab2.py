import numpy as np

from lab1 import get_invert_matrix
    
def main_stage(A, c, x, J_b):
    m, n = A.shape
    J = range(n)
    A_b = A[:, J_b]
    A_b_invert = np.linalg.inv(A_b)

    while True:
        # print(J_b)
        # print(x)
        A_b = A[:, J_b]
        u_ = c[J_b].T.dot(A_b_invert)

        J_nb = [i for i in range(n) if i not in J_b]
        delta= u_.dot(A) - c  
        if np.all(delta[J_nb] >= 0):
            return x, J_b, A_b_invert

        j0 = min([J_nb[i] for i in range(len(J_nb)) if delta[J_nb][i] <= 0])

        z = A_b_invert.dot(A[:,j0])


        if np.all(z <= 0):
            raise Exception("No solution. Infinity count of plans")

        tetta = []

        for i in range(m):
            if z[i] > 0:
                tetta.append(x[J_b[i]] / z[i])
            else:
                tetta.append(np.Infinity)


        tetta_0 = min(tetta)
        s = tetta.index(tetta_0)


        x_new = np.array([0.]*n)
        x_new[j0] = tetta_0

        for i in range(m):
            x_new[J_b[i]] = x[J_b[i]] - tetta_0 * z[i]

        col_number = J_b[s]
        J_b[s] = j0

        x = x_new

        A_b_invert = get_invert_matrix(A_b, A_b_invert, A[:,j0], s)
