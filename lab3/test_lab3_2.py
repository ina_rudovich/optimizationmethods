import numpy as np
from lab3 import simplex_method

A = np.array([
    [1,1,1],
    [2,2,2]
    ])

c = np.array(
    [0, 0, 0])

x = np.array([0,0,0])

J_b = np.array([3,4])

b = np.array([1,0])

# print(main_stage(A, c, x, J_b))

print(simplex_method(A, b, c))