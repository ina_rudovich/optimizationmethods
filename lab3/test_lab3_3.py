import numpy as np
from lab3 import simplex_method

A = np.array([
    [0,1,4,1,0,-3,1,0],
    [1,-1,0,1,0,0,0,0],
    [0,7,-1,0,-1,3,-1,0],
    [1,1,1,1,0,3,-1,1]
    ])

c = np.array(
    [-5,-2, 3, -4, -6, 0, 1, -5])

x = np.array([10, 0, 1.5, 0, 0.5, 0, 0, 3.5])

J_b = np.array([0,2,4,7])

b = np.array([6,10,-2,15])

print(simplex_method(A, b, c))